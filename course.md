# GitLab für Texte - Tutorial

## Welche Texte werden hier betrachtet?

Hier nur ein paar Beispiele für Texte im Bereich Bildung und Wissenschaft. Weitere Gebiete, in denen mit Texten oder Projekten gearbeitet wird, sind natürlich ebenso denkbar ;-)

### Schule

* Unterrichtsvorbereitungen [Vorlage]()
* Skripte mit Anleitungen und Übungen
* Klausurvorbereitungen

### Studium

* Projektarbeiten
* Studienarbeiten
* Bachelorarbeiten
* Masterarbeiten

### Wissenschaft

* Doktorarbeiten
* Veröffentlichungen
* Projektdokumentation

## Was ist GitLab?

GitLab ist eine Webanwendung für die  Versionsverwaltung von - ursprünglich - Software Projekten auf der Basis von Git. [Wikipedia](https://de.wikipedia.org/wiki/GitLab)

Was hat das mit Bildung zu tun? Nicht jeder programmiert! Mit GitLab lassen sich auch Texte für Bildungsmaterialen, Studienarbeiten u.v.a.m. verwalten, gemeinsam bearbeiten und multimedial veröffentlichen - **ohne Kosten, ohne Installation, ohne Infrastruktur**!

GitLab bietet

* Plattform für die Verwaltung von (Text) Projekten
* Hosten von Webseiten
* Versionskontrolle
* Benutzermanagement für Projekte
* Projektmanagement

Mit einem kostenlosen Account auf GitLab lassen sich Projekte in Dateiverzeichnissen online verwalten. Vergleichbar mit einer Dropbox, aber mit vielen weiteren Funktionen, wie Versionierung, Automatisierung und Benutzer- und Rechteverwaltung.

Und so sieht das ganze in der Praxis aus ...

<iframe width="560" height="315" src="https://www.youtube.com/embed/i8MxZU9w6bc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen"></iframe>

Beispiele

* [Ein Beispiel Kurs](https://gitlab.com/TIBHannover/oer/course-metadata-test)
* [Eine Beispiel Studienarbeit](https://gitlab.com/axel-klinger/studienarbeit)


## Ein erstes Projekt - Mein erster Kurs


Mit den folgenden Schritten erstellt man einen Kurs in GitLab.

* [Registrierung/Anmeldung](https://gitlab.com/users/sign_in)
* Projekt anlegen (+ in der Kopfzeile)
  - Name: freie Bezeichnung, präzise und einprägsam
  - Public, um für andere sichtbar zu sein
  - README anlegen, sonst ENTWICKLER-HÖLLE ;-)
* Dokument im Projekt anlegen (+ in der Mitte)
* reinschreiben, wie der Kurs heißt und einen Absatz als Einleitung

Und hier das ganze nochmal im Video ...

<iframe width="560" height="315" src="https://www.youtube.com/embed/7vmyyFigXPQ" frameborder="0" allow="accelerometer" allowfullscreen="allowfullscreen"></iframe>


## Ein zweites Projekt aus Vorlage - Mein erster OER Kurs

### Vorteile

* Ausgabeformate HTML, PDF und EPUB (u.v.a.m.)
* TULLU Kennzeichnung von Wikimedia Commons Bildern
* Maschinenlesbare Metadaten zur Auffindbarkeit
* Lizenzhinweis im Dokument

### Vorbereitung

Meine Vorbereitung für einen Kurs:

* ein Browser und eine Anmeldung auf GitLab
* ein Thema für den Kurs/die Einheit
* ein paar Textschnipsel z.B. aus [Wikipedia](https://de.wikipedia.org/wiki/Wikipedia:Hauptseite)
* ein paar Bilder z.B. von [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page)

### Projekt klonen - ein Template verwenden

1. neues Projekt erstellen -> + -> New Project -> Import project -> Repo by URL

  * [Vorlage](https://gitlab.com/TIBHannover/oer/course-metadata-test.git) (link kopieren) und bei "Git repository URL" einfügen
  * Project name - sprechenden Namen vergeben
  * Project slug - den gleichen Text, aber Leerzeichen durch Bindestriche ersetzen und Umlaute ausschreiben
  * Public machen
  * CREATE PROJECT

1. Metadaten mit [Generator](https://tibhannover.gitlab.io/oer/course-metadata-gitlab-form/metadata-generator.html) erzeugen und kopieren
1. Metadaten in metadata.yml einfügen
1. course.md bearbeiten mit [Markdown](https://gitlab.com/axel-klinger/gitlab-for-documents/blob/master/gitlab-markdown.md)

Nach dem Speichern (Commit changes) dauert es beim ersten mal ca. 15min, bis die generierte Seite unter SETTINGS -> PAGES verfügbar ist (solange gibt es einen 404). Weitere Änderungen stehen i.d.R. nach <1min bereit.

1. Adressen der Ausgabedateien in README anpassen mit SETTINGS -> PAGES -> Your pages are served under
1. Seite aufrufen von README aus  

Und hier das ganze nochmal im Video ...

<iframe width="560" height="315" src="https://www.youtube.com/embed/byRVOPM7qeg" allowfullscreen="allowfullscreen"></iframe>

... und hier das Ergebnis ...

<iframe width="560" height="315" src="https://www.youtube.com/embed/UfC0IWy87AI" allowfullscreen="allowfullscreen"></iframe>

## Die Oberfläche von GitLab

Video zu

* alles einmal angeklickt
* nützliche Features auch für OER

## Zusammenarbeiten mit GitLab

Video zu

* Issues
* Merge-Requests
* Bearbeitungsrechte

## Effizienter offline arbeiten

Offline arbeiten hat einige Vorteile

* man hat immer eine vollständige Kopie auf seinem eigenen Rechner
* viele Text-Editoren haben mächtige Werkzeuge für die Bearbeitung von Markdown und anderen Dateien

[Video mit Atom mehr Komfort für die Arbeit mir GitLab und Markdown]()

Der Nachteil ist

* wenn man nicht sauber synchronisiert, kann das zu Konflikten führen, die lösbar sind, aber Bastelarbeit erfordern

[Video Konflikt durch parallele Arbeit auf Server]

## ANHANG

## Welche Formate haben die Texte?

* **Eingabe :** Unformatierte Texte in [Markdown](gitlab-markdown.md). Später für Fortgeschrittene auch asciidoc oder LaTeX.

* **Ausgabe :** HTML, PDF, EPUB ... und alles was [Pandoc](https://pandoc.org/) unterstützt!

### Warum Versionskontrolle für Texte

Als Beispiel nehmen wir eine Studienarbeit mit ca. 50 Seiten, die in Word - inklusive der Bilder - eine Größe von etwa 10 MB hat. Jedes Mal, wenn wir sie unter einer neuen Version speichern, kommen weitere 10+ MB hinzu, auch wenn nur wenig geändert wurde.

Die gleiche Datei (das GitLab Projekt) hat in diesem Fall in Markdown eine Größe ca. 3 MB, da der größte Teil in Word die Microsoft eigenen Tags im Dokument sind und hier nur der reine Inhalt gespeichert wird. Wenn wir in GitLab eine Änderung einfügen, wird hier nur die Änderung mit wenigen KB als Version gespeichert. Es werden nicht alle enthaltenen Bilder und das ganze Dokument jedes mal wieder neu gespeichert.

Weitere Vorteile sind u.a.

* mit Markdown: Trennung von Inhalt und Layout unterstützt die Nachnutzbarkeit
* detaillierte Historie der Änderungen


### Das Format Markdown

[Übersicht über GitLab-Markdown](https://docs.gitlab.com/ee/user/markdown.html)


### Lizeninformationen unter Bilder einfügen

Die CC Lizenzen basieren auf einem [Dreischichten-Konzept](https://creativecommons.org/licenses/):

- Lizenzvertrag: Traditionelle Lizenz, die in der Ausdrucksweise der Juristen gehalten ist.
- Commons Deed: Die menschenlesbare Lizenz, die auf praktische Art und Weise von Lizenzgeber:innen und Lizenznehmer:innen verwendetet werden kann
- CC Rights Expression Language ([CC REL](https://wiki.creativecommons.org/wiki/CC_REL): Maschinenlesbare Lizenz für Suchmaschinen und Software.

All images should be organized in the images directory- general images at the top and use a subdirectory for the modules. Please name all images with simple, clear names, preferably lower case like fair-use-4-factors.jpg or oer-remix-game.jpg

The Markdown for the image should include the code to display the image and a second line for attribution. Here are some examples (note sample code has line breaks so you can see, each line of markdown should be a single line!)

#### Easy to implement, but not machine readable

```html
![Brown Cat With Green Eyes]((https://i.imgur.com/WiYXnnp.jpg) "Brown Cat With Green Eyes")

*"[Brown Cat With Green Eyes](https://i.imgur.com/WiYXnnp.jpg)" by [Kelvin Valerio](https://www.pexels.com/@kelvin809) licensed under [CC 0](https://creativecommons.org/publicdomain/zero/1.0/) from [Pexels](https://www.pexels.com/photo/adorable-animal-blur-cat-617278/)*
```

![Brown Cat With Green Eyes](https://i.imgur.com/ukhHgII.jpg)

*"[Brown Cat With Green Eyes](https://i.imgur.com/ukhHgII.jpg)" by [Kelvin Valerio](https://www.pexels.com/@kelvin809) licensed under [CC 0](https://creativecommons.org/publicdomain/zero/1.0/) from [Pexels](https://www.pexels.com/photo/adorable-animal-blur-cat-617278/)*

#### Nicht so leicht einzufügen, aber maschinenlesbar  (und CC REL-konform)

~~~html
<div about="https://i.imgur.com/WiYXnnp.jpg">

![<span property="dc:title">Brown Cat With Green Eyes</span> 
by 
<a rel="cc:attributionURL dc:creator" href="https://www.pexels.com/@kelvin809" property="cc:attributionName">Kelvin Valerio</a> 
under 
<a rel="license" href="https://creativecommons.org/publicdomain/zero/1.0/">CC 0</a> 
from 
<a rel="dc:source"
href="https://i.imgur.com/ukhHgII.jpg">Pexels</a>](https://i.imgur.com/ukhHgII.jpg)
</div>
~~~

<div about="https://i.imgur.com/WiYXnnp.jpg">

![<span property="dc:title">Brown Cat With Green Eyes</span> 
by 
<a rel="cc:attributionURL dc:creator" href="https://www.pexels.com/@kelvin809" property="cc:attributionName">Kelvin Valerio</a> 
under 
<a rel="license" href="https://creativecommons.org/publicdomain/zero/1.0/">CC 0</a> 
from 
<a rel="dc:source"
href="https://i.imgur.com/ukhHgII.jpg">Pexels</a>](https://i.imgur.com/ukhHgII.jpg)
</div>

### Google Docs zu Markdown konvertieren

Mit Hilfe des Google Apps script [gdocs2md](https://github.com/mangini/gdocs2md) können Google Drive Dokumente zu Markdown konvertiert werden. So geht es:

1. Das Google Doc aufrufen.
1. Das [Google Apps Skript](https://raw.githubusercontent.com/mangini/gdocs2md/master/converttomarkdown.gapps) kopieren.
1. Über den Reiter "Tools" den Skripteditor aufrufen
1. Die Template-Funktion löschen und das Google Apps Skript einfügen.
1. Das Skript speichern (bspw. "Markdown-Export-Skript").
1. Wieder das Google Doc öffnen und über "Tool" den Skripteditor öffnen.
1. Über "Select function" "Convert to Markdown" wählen
1. Die Markdown Dateien und darin enthaltenen Bilder werden an die eigene Google-Mail-Adresse gesendet.

### Effizienter offline arbeiten

* Am Beispiel Atom (andere Editoren mit guter Markdown Unterstützung und Git-Anbindung sind ebenso möglich, wie z.B. Visual Studio Code oder auch diverse Apps für GitHub/GitLab)

[Video zur Offline-Arbeit mit Atom]()

### Konflikte Lösen!

> TODO !!!

### Grundlagen von Git

* Installation

#### Die Elemente

* Repository
* Working Direktory
* Commit
* Index
* Branch

#### Die Funktionen

Die folgende Auflistung bekommt man mit dem Aufruf von `git` ohne Parameter

```
start a working area (see also: git help tutorial)
   clone      Clone a repository into a new directory
   init       Create an empty Git repository or reinitialize an existing one

work on the current change (see also: git help everyday)
   add        Add file contents to the index
   mv         Move or rename a file, a directory, or a symlink
   reset      Reset current HEAD to the specified state
   rm         Remove files from the working tree and from the index

examine the history and state (see also: git help revisions)
   bisect     Use binary search to find the commit that introduced a bug
   grep       Print lines matching a pattern
   log        Show commit logs
   show       Show various types of objects
   status     Show the working tree status

grow, mark and tweak your common history
   branch     List, create, or delete branches
   checkout   Switch branches or restore working tree files
   commit     Record changes to the repository
   diff       Show changes between commits, commit and working tree, etc
   merge      Join two or more development histories together
   rebase     Reapply commits on top of another base tip
   tag        Create, list, delete or verify a tag object signed with GPG

collaborate (see also: git help workflows)
   fetch      Download objects and refs from another repository
   pull       Fetch from and integrate with another repository or a local branch
   push       Update remote refs along with associated objects
```
