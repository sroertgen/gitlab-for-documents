# GitLab Markdown

Zusammenfassung der wesentlichen Elenente für Texte aus der offiziellen Dokumentation [GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html)

## Überschriften

Markdown
```
# Überschrift 1
## Überschrift 2
### Überschrift 3
```

Ausgabe
# Überschrift 1
## Überschrift 2
### Überschrift 3


## Absätze

Markdown
```
Eine Zeile ist bis zum Zeilenumbruch ein Absatz.
Ohne Lehrzeile.

Mit Lehrzeile.
```

Ausgabe

Eine Zeile ist bis zum Zeilenumbruch ein Absatz.
Ohne Lehrzeile.

Mit Lehrzeile.

## Formatierungen

### Fett

Markdown

```
**Fettschrift**
```

Ausgabe

**Fettschrift**

### Italic

Markdown

```
*Italicschrift*
```

Ausgabe

*Italicschrift*


## Zitate

Markdown

```
> Ein Zitat
```

Ausgabe

> Ein Zitat

## Listen

### Ohne Nummern

Markdown

```
* Liste 1
* Liste 2
* Liste 3
```

Ausgabe

* Liste 1
* Liste 2
* Liste 3

### Mit Nummern

Markdown

```
1. Liste 1
1. Liste 2
1. Liste 3
```

Ausgabe

1. Liste 1
1. Liste 2
1. Liste 3

## Links

Markdown

```
[Gitlab](http://gitlab.com)
```

Ausgabe

[Gitlab](http://gitlab.com)

## Bilder

Markdown

```
![Beschreibung](http://placehold.it/100x100)
```

Ausgabe

![Beschreibung](http://placehold.it/100x100)

## Videos

Beim Einbinden von Videos müssen momentan zwei Dinge beachtet werden:

1. Wenn das Video direkt in GitLab angezeigt werden soll, gibt es hier momentan noch keine Vorschau-Funktion (bei [hackmd.io](https://hackmd.io) geht das aber).

Markdown
```
![Beispiel Video](https://www.youtube.com/watch?v=7vmyyFigXPQ)
```

Ausgabe

![Beispiel Video](https://www.youtube.com/watch?v=7vmyyFigXPQ)


2. Wenn Du das Dokument in HTML oder EPUB automatisch konvertieren lässt, werden Videos, wenn Du sie wie folgt einbettest, mit einer Vorschau angezeigt. Kopiere dazu einfach den Link, den du von Youtube bekommst, wenn Du auf "Teilen" und dann "Einbetten" klickst:

``` 
<iframe width="560" height="315" src="https://www.youtube.com/embed/7vmyyFigXPQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
``` 


## Tabellen

Markdown
```
| header 1 | header 2 | header 3 |
| ---      |  ------  |---------:|
| cell 1   | cell 2   | cell 3   |
| cell 4 | cell 5 is longer | cell 6 is much longer than the others, but that's ok. It will eventually wrap the text when the cell is too large for the display size. |
| cell 7   |          | cell <br> 9 |
```

Ausgabe

| header 1 | header 2 | header 3 |
| ---      |  ------  |---------:|
| cell 1   | cell 2   | cell 3   |
| cell 4 | cell 5 is longer | cell 6 is much longer than the others, but that's ok. It will eventually wrap the text when the cell is too large for the display size. |
| cell 7   |          | cell <br> 9 |

## Formeln

Markdown
```
\```math
a^2+b^2=c^2
\```
```

Ausgabe

```math
a^2+b^2=c^2
```

## Quellcode

Markdown
```
```

Ausgabe

## Diagramme

Markdown
```
```

Ausgabe
