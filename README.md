# Gitlab für Texte

... in Schule, Studium und Wissenschaft!

## Warum und wie sieht das aus/wie fühlt sich das an?

[![Entwicklungsumgebung mit Groovy/Git testen](https://img.youtube.com/vi/i8MxZU9w6bc/maxresdefault.jpg)](https://youtu.be/i8MxZU9w6bc)


## Klingt gut? - Hier gehts zum Kurs in [HTML](https://sroertgen.gitlab.io/gitlab-for-documents/index.html), [PDF](https://sroertgen.gitlab.io/gitlab-for-documents/course.pdf) oder als [EPUB](https://sroertgen.gitlab.io/gitlab-for-documents/course.epub)
